<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends BaseController{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		$this->data['title'] = 'Новости';
		$listNews = News::orderBy('idate', 'DESC')->paginate(20);

		return view('pages.news.list', ['listNews' => $listNews, 'data' => (object) $this->data]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\News $news
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(News $news){
		$this->data['title'] = $news->title;
		return view('pages.news.show', ['news' => $news, 'data' => (object) $this->data]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		return redirect()->route('news.index');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\Models\News         $news
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, News $news){
		return redirect()->route('news.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\News $news
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(News $news){
		return redirect()->route('news.index');
	}
}
