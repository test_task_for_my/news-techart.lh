<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News create($value)
 *
 * @property integer $id
 * @property integer $idate
 * @property string  $title
 * @property string  $announce
 * @property string  $content
 */
class News extends Model{
	protected $table = 'news';
	protected $primaryKey = 'id';
}
