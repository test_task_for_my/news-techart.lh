@extends('layouts.app')
<?
/**
 * @var  \Illuminate\Database\Eloquent\Collection $listNews
 */
?>
@section('content')
	<div class="row">
		<div class="col-xs-12">
			@if(isset($listNews) and  $listNews->count() >0)
				@if($listNews->hasPages())
					<ul class="pagination pull-links">{{$listNews->links()}}</ul>
				@endif

				<div class="list-group">
					@foreach($listNews as $news)
						<div class="list-group-item">
							<a href="{{route('news.show',['news'=>$news->id])}}" title="{{$news->title}}">
								{{date('d-m-Y H:i',$news->idate)}} {!! $news->title !!}</a>
							<p>{!! $news->announce !!}</p>
							<a href="{{route('news.show',['news'=>$news->id])}}" title="{{$news->title}}">Подробнее >></a>
						</div>
					@endforeach
				</div>

				@if($listNews->hasPages())
					<ul class="pagination pull-links">{{$listNews->links()}}</ul>
				@endif

			@else
				<p class="alert alert-info">Список новостей пуст</p>
			@endif
		</div><!-- /.col -->
	</div>
@endsection
