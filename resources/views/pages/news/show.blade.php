@extends('layouts.app')
<?
/**
 * @var  \Illuminate\Database\Eloquent\Collection $listNews
 */
?>
@section('content')
	<div class="row">
		<div class="col-xs-12">
			@if(isset($news))
				<p>{!! $news->announce !!}</p>
				<p>{{date('d-m-Y H:i',$news->idate)}}</p>
				<hr>
				<div>{!! $news->content !!}</div>
			@else
				<p class="alert alert-info">Описание новости отсутствует</p>
			@endif
		</div><!-- /.col -->
	</div>
@endsection
