<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{$data->title or 'Главная'}}</title>

	<link href="{{asset('/css/app.css')}}" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-12">
			<nav>
				<ul class="nav">
					<li class="nav-item"><a href="{{route('index')}}" title="Главная" class="nav-link">Главная</a></li>
					<li class="nav-item"><a href="{{route('news.index')}}" title="Новости" class="nav-link">Новости</a></li>
				</ul>
			</nav>
			{{ Breadcrumbs::render() }}
{{--			{{ Breadcrumbs::render('news.index') }}--}}
			<h1>{{$data->title or 'Главная'}}</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<section id="content">
				@yield('content')
			</section>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<footer>
				<p class="text-center">Задание предоставленно компанией TechArt</p>
				<p class="text-center">Задание выполнил Ковалёв Тарас Викторович</p>
			</footer>
		</div>
	</div>
</div>

<script src="{{asset('/js/app.js')}}" type="text/javascript"></script>
</body>
</html>
