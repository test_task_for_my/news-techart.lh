@if (isset($breadcrumbs))
	@if (count($breadcrumbs))
		<ul class="breadcrumb">
			@foreach ($breadcrumbs as $i => $breadcrumb)
				@if ($breadcrumb->url && !$loop->last)
					<li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">
							@if($i==0)<i class="fa fa-home"></i>@endif {{ $breadcrumb->title }}</a></li>
				@else
					<li class="breadcrumb-item active">@if($i==0)<i class="fa fa-home"></i>@endif {{ $breadcrumb->title }}</li>
				@endif

			@endforeach
		</ul>
	@endif
@endif
