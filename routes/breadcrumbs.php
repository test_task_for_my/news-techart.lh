<?php

// Home
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// SEE https://github.com/davejamesmiller/laravel-breadcrumbs

// admin.index
Breadcrumbs::register('index', function(BreadcrumbsGenerator $breadcrumbs){
	$breadcrumbs->push('Главная', route('index'));
});


Breadcrumbs::register('news.index', function(BreadcrumbsGenerator $breadcrumbs){
	$breadcrumbs->parent('index');
	$breadcrumbs->push('Список адресов', route('news.index'));
});

Breadcrumbs::register('news.show', function(BreadcrumbsGenerator $breadcrumbs, \App\Models\News $news){
	$breadcrumbs->parent('news.index');
	$breadcrumbs->push('Добавление адреса', route('news.show', ['news' => $news->id]));
});
